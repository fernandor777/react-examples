import React, { Component } from 'react';
import 'primereact/resources/themes/omega/theme.css';
import 'font-awesome/css/font-awesome.css';
import 'primereact/resources/primereact.min.css';
import {Calendar} from 'primereact/components/calendar/Calendar';


class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
          date: null
        };
    };
    render() {
        return (
            <form>
                <div className="ui-g">
                    <div className="ui-g-12 ui-md-6 ui-lg-3">
                        <Calendar value={this.state.date} onChange={(e) => this.setState({date: e.value})}></Calendar> 
                    </div>
                </div>
            </form>
        );
    }
}

export {Login};



