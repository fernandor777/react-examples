import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import 'primereact/resources/themes/omega/theme.css';
import 'font-awesome/css/font-awesome.css';
import 'primereact/resources/primereact.min.css';
import {Login} from './Login';
import { connect } from "react-redux";

export const mapStateToProps = state => {
  return { tweets: state.tweets };
};

export const timeline = ({tweets}) => (
    <ul>
        {tweets.map( el => (
            <li>
                {el.content}
            </li>
            )
        )}
    </ul>
);

export const List = connect(mapStateToProps)(timeline);

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          Hi...
        </p>
        <List />
      </div>
    );
  }
}



export default App;


